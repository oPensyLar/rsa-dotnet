function SplitInBytes(str, init, final)
{

  var strRet = "";
  var toBytes = NaN;

  if(final == -1)
    toBytes = str.length;

    else
      toBytes = final;

  for(i = init; i < toBytes; i++)
  {    
    strRet = strRet + str[i];
  }

  return strRet;
}

function toHex(rawData)
{

  var strRet = new Array(rawData.length * 2);
  var hexAlphabet = "0123456789ABCDEF";

  for(i = 0; i < rawData.length; i++)
  {

    hex1 = rawData.charCodeAt(i) >> 4;
    indx1 = i * 2;

    hex2 = rawData.charCodeAt(i) & 0xF;
    indx2 = i * 2 + 1;

    strRet[indx1] = hexAlphabet[hex1];
    strRet[indx2] = hexAlphabet[hex2];

  }

  return strRet.join("");

}

function VerifyJws(strPem, strPlainText, hexSign)
{

  var sign = new KJUR.crypto.Signature({"alg": "SHA256withRSA"});  
  sign.init(strPem);
  sign.updateString(strPlainText);
  var isValid = sign.verify(hexSign);

  return isValid;
}


function GenPemForge(n, e)
{
   
   var publicKey = forge.pki.setRsaPublicKey(
         parseBigInteger(n),
         parseBigInteger(e));

   var pem = forge.pki.publicKeyToPem(publicKey);

   return pem;
}

function ParseInfoXml(sXml)
{

    parser = new DOMParser();

    xmlDoc = parser.parseFromString(sXml, "text/xml");

    modulus = xmlDoc.getElementsByTagName("Modulus")[0].childNodes[0].nodeValue;

    exponent = xmlDoc.getElementsByTagName("Exponent")[0].childNodes[0].nodeValue;

  return {"exponent" : exponent, "modulus": modulus};
}

function GenPem(keyStrB64)
{

    var keyBlob = System.Convert.FromBase64String(keyStrB64);
    var rsa = new System.Security.Cryptography.RSACryptoServiceProvider(2048);

    rsa.ImportCspBlob(keyBlob);

    var strXml = rsa.ToXmlString(false);
    var infoRsa = ParseInfoXml(strXml);

    var strPem = GenPemForge(infoRsa.modulus, infoRsa.exponent);

  return strPem;
}

function GetNewRsaProvider(dwKeySize)
{

  if (!dwKeySize) dwKeySize = 512;

  return new System.Security.Cryptography.RSACryptoServiceProvider(dwKeySize);
}


function parseBigInteger(b64)
{
    return new BigInteger(forge.util.createBuffer(forge.util.decode64(b64)).toHex(), 16);
}

function ReadUrlSafeBase64(urlSafeBase64)
{

  var incoming = urlSafeBase64.split("_").join("/");
  incoming = incoming.split("-").join("+");


  switch (urlSafeBase64.length % 4)
  {
      case 2: incoming += "=="; break;
      case 3: incoming += "="; break;
  }

  return incoming;  

}

function IsNullOrWhiteSpace( input )
{
  return !input || !input.trim();
}

function AES(dataEncrypt, pKey, pIv)
{

  var retDecrypted = CryptoJS.AES.decrypt(dataEncrypt, pKey, {
    iv: pIv,
    padding: CryptoJS.pad.Pkcs7,
    mode: CryptoJS.mode.CBC

  });

  return retDecrypted;
}

function PBKDF2(aesSymm)
{

  var keyconiv = CryptoJS.PBKDF2(aesSymm, aesSymm, {keySize: 12,
  iterations: 1000
 });

  keyconiv.clamp();

  return keyconiv;
}

function TestVerify(sPubKey, sPlainText, hexSign)
{

  var sign = new KJUR.crypto.Signature({"alg": "SHA256withRSA"});  
  sign.init(sPubKey);
  sign.updateString(sPlainText);
  var isValid = sign.verify(hexSign);

  return 0;

}

function VerifySignature(sampleData, sampleSymmetricKey, samplePublicKey)
{

  var retCode;
  var numeroDeCertificado;
  
  if(!IsNullOrWhiteSpace(sampleData))
  {

    partesSello = sampleData.split('|');

    if(partesSello.length > 0)
    {

      var numeroDeCertificado = partesSello[0];
      var selloBase64 = partesSello[1];
      var dEncrypt = ReadUrlSafeBase64(selloBase64);

      var keyAndIv = PBKDF2(sampleSymmetricKey);

      var key = CryptoJS.lib.WordArray.create(keyAndIv.words.slice(0, 8));
      var iv = CryptoJS.lib.WordArray.create(keyAndIv.words.slice(8));
      
      var aesData = AES(dEncrypt, key, iv);
      
      var plainText = aesData.toString(CryptoJS.enc.Latin1);

      var strRawSign = SplitInBytes(plainText, 0, 256);
      var strPlainText = SplitInBytes(plainText, 256, -1);

      strPem = GenPem(samplePublicKey);
      
      var hexaSign = toHex(strRawSign);
      var verifySign = VerifyJws(strPem, strPlainText, hexaSign);

      return {"valid": verifySign, "certificateNumber" : numeroDeCertificado, "data": strPlainText};

    }


  }

  return 0;

}

function RunTest()
{

  var sData = document.form1.sampleData.value;
  var sSymmetricKey = document.form1.sampleSymmetricKey.value;
  var sPublicKey = document.form1.samplePublicKey.value;
  var sPlainText = document.form1.plain_text.value;
  var hexaSign = document.form1.hexa_sign.value;

  // TestVerify(sPublicKey, sPlainText, hexaSign);
  var retValues = VerifySignature(sData, sSymmetricKey, sPublicKey);

  console.log(retValues);

  return 0;
}
